package main

import (
	"fmt"
	pgs "gitlab.com/melvin.biamont/go-pgs-parser"
	"os"
)

func main() {
	parser := pgs.NewPgsParser()

	err := parser.ConvertToPngImages("./sample/input.sup", func(index int) (*os.File, error) {
		return os.Create(fmt.Sprintf("./sample/subs/input.%d.png", index))
	})
	if err != nil {
		panic(err)
	}
}
