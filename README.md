# go-pgs-parser

Golang PGS Parser

## Installation

```shell
go get gitlab.com/melvin.biamont/go-pgs-parser
```

## Getting started

The parser requires you specify a SUP file's path.

More info about PGS and SUP file here: https://fileinfo.com/extension/sup

For each subtitle bitmap, it'll ask you in which file to write it.

### Save as PNG images (recommended)

```go
package main

import (
	"fmt"
	pgs "gitlab.com/melvin.biamont/go-pgs-parser"
	"os"
)

func main() {
	parser := pgs.NewPgsParser()

	parser.ConvertToPngImages("./sample/input.sup", func(index int) (*os.File, error) {
		return os.Create(fmt.Sprintf("./sample/subs/input.%d.png", index))
	})
}
```
### Save as JPG images

```go
package main

import (
	"fmt"
	pgs "gitlab.com/melvin.biamont/go-pgs-parser"
	"os"
)

func main() {
	parser := pgs.NewPgsParser()

	parser.ConvertToJpgImages("./sample/input.sup", func(index int) (*os.File, error) {
		return os.Create(fmt.Sprintf("./sample/subs/input.%d.jpg", index))
	})
}
```


## Extract SUP from MKV

You can extract a SUP file using ffmpeg like this:

```shell
ffmpeg -i input.mkv -map 0:s:0 -c copy output.sup -y
```