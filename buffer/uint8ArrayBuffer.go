package buffer

import "errors"

type Uint8BArrayBuffer struct {
	buffer []byte
	BufferAdapter
}

func NewUint8ArrayBuffer(buffer []byte) *Uint8BArrayBuffer {
	return &Uint8BArrayBuffer{
		buffer: buffer,
	}
}

func (u Uint8BArrayBuffer) Length() int {
	return len(u.buffer)
}

func (u Uint8BArrayBuffer) At(index int) (int, error) {
	if index < len(u.buffer) {
		return int(u.buffer[index]), nil
	}

	return 0, errors.New("index out of bounds")
}

func (u Uint8BArrayBuffer) SubArray(start int, end int) BufferAdapter {
	return NewUint8ArrayBuffer(u.buffer[start:end])
}
